package stock.management

import groovy.transform.ToString

@ToString(includes = ['size', 'price'])
class Variation {
    static hasMany = [products: Product]
    static belongsTo = Product

    String size
    String price

    static constraints = {
        size unique: ['price']
    }

    @Override
    String toString() {
        "${size}: ${price}"
    }
}
