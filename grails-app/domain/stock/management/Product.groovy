package stock.management

class Product {
    static hasMany = [variations: Variation]
    String foreignId
    String name
    Brand brand
    Integer amount

    static constraints = {
        foreignId blank: false, unique: true
        name blank: false
        brand nullable: true
    }

    @Override
    String toString() {
        "${brand}. ${name}"
    }
}
