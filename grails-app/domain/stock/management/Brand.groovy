package stock.management

class Brand {
    String name

    static constraints = {
        name blank: false, unique: true
    }

    @Override
    String toString() {
        name;
    }
}
