<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'variation.label', default: 'Variation')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-variation" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <sec:ifAnyGranted roles='ROLE_ADMIN'>
            <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        </sec:ifAnyGranted>
    </ul>
</div>

<div id="show-variation" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list variation">

        <li class="fieldcontain">
            <span id="size-label" class="property-label">Size</span>
            <div class="property-value"><f:display bean="variation" property="size"/></div>
        </li>

        <li class="fieldcontain">
            <span id="price-label" class="property-label">Price</span>
            <div class="property-value"><f:display bean="variation" property="price"/></div>
        </li>

    </ol>
    <sec:ifAnyGranted roles='ROLE_ADMIN'>
        <g:form resource="${this.variation}" method="DELETE">
            <fieldset class="buttons">
                <g:link class="edit" action="edit" resource="${this.variation}"><g:message
                        code="default.button.edit.label" default="Edit"/></g:link>
                <input class="delete" type="submit"
                       value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </fieldset>
        </g:form>
    </sec:ifAnyGranted>
</div>
</body>
</html>
