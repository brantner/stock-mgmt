<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>
        <g:layoutTitle default="Stock management"/>
    </title>

    <asset:stylesheet src="application.css"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <g:layoutHead/>
</head>

<body>

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Stock Management</a>
        </div>

        <sec:ifLoggedIn>
            <ul class="nav navbar-nav">
                <li><g:link controller="product">Products</g:link></li>
                <li><g:link controller="brand">Brands</g:link></li>
                <li><g:link controller="variation">Variations</g:link></li>
                <li><g:link controller="product" action="remains">Remains</g:link></li>
            </ul>

            <g:form controller="product" action="search" class="navbar-form navbar-left">
                <div class="form-group">
                    <input class="form-control" type="text" name="searchQuery" placeholder="Product name or brand">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </g:form>

            <ul class="nav navbar-nav navbar-right">
                <li><g:link controller='logout'>Logout</g:link></li>
            </ul>
        </sec:ifLoggedIn>
    </div>
</nav>

<g:layoutBody/>

<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>

</body>
</html>
