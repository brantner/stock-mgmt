<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Stock Management</title>
</head>

<body>

<div id="content" role="main" class="container">
    <section class="row colset-2-its">
        <div class="col-md-9">
            <ul class="nav nav-pills">
                <li role="presentation"><g:link controller="product">Products</g:link></li>
                <li role="presentation"><g:link controller="brand">Brands</g:link></li>
                <li role="presentation"><g:link controller="variation">Variations</g:link></li>
            </ul>
        </div>
    </section>
</div>

</body>
</html>
