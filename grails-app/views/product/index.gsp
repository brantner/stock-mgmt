<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <sec:ifAnyGranted roles='ROLE_ADMIN'>
            <li><g:link class="create" action="create"><g:message code="default.add.label"
                                                                  args="['Products']"/></g:link></li>
        </sec:ifAnyGranted>

    </ul>
</div>

<div id="list-product" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <f:table collection="${productList}"/>

    <div class="pagination">
        <g:paginate total="${productCount ?: 0}"/>
    </div>
</div>
<g:if test="${searchQuery && productCount > 0}">
    <g:link action="downloadSearchResults" params="[searchQuery: searchQuery]">Download results</g:link>
</g:if>
</body>
</html>