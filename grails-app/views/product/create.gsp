<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>
<a href="#create-product" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<g:hasErrors bean="${this.product}">
    <ul class="errors" role="alert">
        <g:eachError bean="${this.product}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                    error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>

<div class="container">
    <div class="row row-eq-height">
        <div class="col-md-6">
            <div id="create-product" class="content scaffold-create" role="main">
                <h1><g:message code="default.create.label" args="[entityName]"/></h1>
                <g:form action="save">
                    <fieldset class="form">
                        <f:all bean="product"/>
                    </fieldset>
                    <fieldset class="buttons">
                        <g:submitButton name="create" class="save"
                                        value="${message(code: 'default.button.create.label')}"/>
                    </fieldset>
                </g:form>
            </div>
        </div>

        <div class="col-md-6">
            <div class="content scaffold-create" role="main">
                <h1>Bulk upload</h1>
                <g:uploadForm name="productBulk" controller="product" action="saveBulk">
                    <fieldset class="form">
                        <input type="file" name="productsFile"/>
                    </fieldset>
                    <fieldset class="buttons bulk-upload">
                        <g:submitButton name="create" class="save"
                                        value="${message(code: 'default.button.upload.label')}"/>
                    </fieldset>
                </g:uploadForm>

            </div>
        </div>
    </div>
</div>

</body>
</html>
