package stock.management

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'product')
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
