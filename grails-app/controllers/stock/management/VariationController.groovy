package stock.management

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class VariationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Variation.list(params), model: [variationCount: Variation.count()]
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def show(Variation variation) {
        respond variation
    }

    @Secured('ROLE_ADMIN')
    def create() {
        respond new Variation(params)
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def save(Variation variation) {
        if (variation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (variation.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond variation.errors, view: 'create'
            return
        }

        variation.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'variation.label', default: 'Variation'), variation.id])
                redirect variation
            }
            '*' { respond variation, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN')
    def edit(Variation variation) {
        respond variation
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def update(Variation variation) {
        if (variation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (variation.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond variation.errors, view: 'edit'
            return
        }

        variation.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'variation.label', default: 'Variation'), variation.id])
                redirect variation
            }
            '*' { respond variation, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def delete(Variation variation) {

        if (variation == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        variation.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'variation.label', default: 'Variation'), variation.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'variation.label', default: 'Variation'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
