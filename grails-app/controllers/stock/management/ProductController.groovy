package stock.management

import com.google.common.io.Files
import grails.plugin.quickSearch.QuickSearchService
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import pl.touk.excel.export.WebXlsxExporter

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ProductController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    ProductImportService productImportService
    QuickSearchService quickSearchService

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Product.list(params), model: [productCount: Product.count()]
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def search() {
        def foundProducts = findProducts(params.searchQuery)
        render(view: 'index', model: [productList : findProducts(params.searchQuery),
                                      productCount: foundProducts.size(),
                                      searchQuery : params.searchQuery])
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def downloadSearchResults() {
        def withProperties = ['foreignId', 'name', 'brand.name', 'amount']

        new WebXlsxExporter().with {
            setResponseHeaders(response)
            add(findProducts(params.searchQuery), withProperties)
            save(response.outputStream)
        }
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def remains() {
        def remains = Product.findAllByAmountLessThan(5)
        render(view: 'index', model: [productList: remains, productCount: remains.size()])
    }

    @Secured(value = ['ROLE_ADMIN', 'ROLE_READ_ONLY'])
    def show(Product product) {
        respond product
    }

    @Secured('ROLE_ADMIN')
    def create() {
        respond new Product(params)
    }

    @Secured(value = 'ROLE_ADMIN', httpMethod = 'POST')
    @Transactional
    def saveBulk() {
        def file = params.productsFile
        def cells
        switch (Files.getFileExtension(file.filename)) {
            case 'xls':
                cells = productImportService.importXls(file.inputStream)
                break;
            case 'csv':
                cells = productImportService.importCsv(file.inputStream)
                break;
            default:
                def product = new Product()
                product.errors.reject('product.file.type.unknown')
                respond product.errors, view: 'create'
                return
        }

        for (cell in cells) {
            def product = new Product(cell)
            if (!product.validate()) {
                transactionStatus.setRollbackOnly()
                respond product.errors, view: 'create'
                return
            }
            product.save()
        }

        redirect(action: 'index')
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def save(Product product) {
        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view: 'create'
            return
        }

        product.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN')
    def edit(Product product) {
        respond product
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def update(Product product) {
        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (product.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond product.errors, view: 'edit'
            return
        }

        product.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect product
            }
            '*' { respond product, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def delete(Product product) {

        if (product == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        product.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'product.label', default: 'Product'), product.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    private def findProducts(String query) {
        def searchParams = [:]
        def searchProperties = [name: 'name', brand: 'brand.name']
        quickSearchService.search(domainClass: Product, query: params.searchQuery,
                searchParams: searchParams, searchProperties: searchProperties)
    }
}
