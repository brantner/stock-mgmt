package stock.management

import org.apache.commons.csv.CSVParser
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.grails.plugins.excelimport.ExcelImportService

import static org.apache.commons.csv.CSVFormat.DEFAULT

class ProductImportService {

    ExcelImportService excelImportService;

    private static Map CONFIG_XLS_COLUMN_MAP = [
            sheet    : 'Sheet1',
            startRow : 0,
            columnMap: ['A': 'foreignId',
                        'B': 'name',
                        'C': 'amount']
    ]


    def importXls(InputStream inputStream) {
        excelImportService.convertColumnMapConfigManyRows(WorkbookFactory.create(inputStream), CONFIG_XLS_COLUMN_MAP)
    }

    def importCsv(InputStream inputStream) {
        def records = []

        inputStream.withReader { reader ->
            CSVParser parser = new CSVParser(reader, DEFAULT.withHeader('foreignId', 'name', 'amount'))
            for (record in parser.iterator()) {
                records << record.toMap()
            }
        }

        records
    }
}
