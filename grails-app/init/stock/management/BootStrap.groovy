package stock.management

import stock.management.auth.Role
import stock.management.auth.User
import stock.management.auth.UserRole

class BootStrap {

    def init = { servletContext ->
        def adminRole = new Role(authority: 'ROLE_ADMIN').save()
        def readOnlyRole = new Role(authority: 'ROLE_READ_ONLY').save()

        def adminUser = new User(username: 'admin', password: 'admin').save()
        def readOnlyUser = new User(username: 'ro', password: 'ro').save()

        UserRole.create adminUser, adminRole
        UserRole.create readOnlyUser, readOnlyRole

        UserRole.withSession {
            it.flush()
            it.clear()
        }

        assert User.count() == 2
        assert Role.count() == 2
        assert UserRole.count() == 2
    }
    def destroy = {
    }
}
